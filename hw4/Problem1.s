	.text

	.globl main
	.globl vec_sum_sq

main:
	la $a0, result
	la $a1, vec1
	la $a2, vec2
	la $t0, n_elts
	lw $a3, 0($t0)

	# save return address
	addi $sp, $sp, -4
	sw $ra, 0($sp)

	# call vec_sum_sq
	jal vec_sum_sq

	# restore return address
	lw $ra, 0($sp)
	addi $sp, $sp, 4

	# return from main
	jr $ra

# $a0 is base address of result vector.
# $a1 is base address of first argument vector.
# $a2 is base address of second argument vector.
# $a3 is the number of elements in the argument vectors.

# vec_sum_sq:
#	li $t1, 0

# loop: 
#	beq $t1, $a3, exit_func
#	lw $t2, ($a1)
#	mult $t2, $t2
#	mflo $t2

#	lw $t3, ($a2)
#	mult $t3, $t3
#	mflo $t3

#	add $t4, $t2, $t3
#	sw $t4, ($a0)

#	addi $a1, $a1, 4
#	addi $a2, $a2, 4
#	addi $a0, $a0, 4
#	addi $t1, $t1, 1
#	j loop

#exit_func:
#	jr $ra


vec_sum_sq:
	li $t1, 0

loop: 
	beq $t1, $a3, exit_func
	lw $t2, ($a1)
	lw $t3, ($a2)
	
	mult $t2, $t2
	mflo $t2

	mult $t3, $t3
	mflo $t3

	add $t4, $t2, $t3
	sw $t4, ($a0)

	addi $a1, $a1, 4
	addi $a2, $a2, 4
	addi $a0, $a0, 4
	addi $t1, $t1, 1
	j loop


exit_func:
	jr $ra

########################################################################
# Data segment: you are free to change the value of the
# n_elt variables and the contents of the vec1 and vec2 arrays
# for testing purposes.

	.data
result:	.space 10000
#n_elts:	.word 2
#vec1:	.word 1, 2
#vec2:	.word 6, 7

n_elts:	.word 5
vec1:	.word 1, 2, 3, 4, 5
vec2:	.word 6, 7, 8, 9, 10