Soo Hyun Lee 
slee387
600.229 CSF
HW4 - README

Problem 1: 
Rationale

Within vec_sum_sq, $t2 & $t3 are going through very similar procedures - but are unrelated ot each other (at least until later). So these two sets of instructions can be pipelined. They access two different a registers and calculate / output result into unrelated registers. So originally, I put all the $t2 instructions together and $t3 instructions afterwards (the commented out section of the code shows the original code). But since mult would write to LO, I couldn't pipeline those together. Adding $t2, $t3 into $t4 had to wait for the results of $t2 and $t3 to be processed. Then, incrementing the values in the registers ($a1, $a2, $a0, $t1) all required the same stages, so I couldn't optimise them beyond just having them take place one after another. There wasn't a lot to optimise, so I'm not sure if it really sped up the process by a lot. 



Problem 2: 

Note (cache - reading hex address): I initially handled 8 bits of address, but when I saw 7 bits for one of the write files, I wrote out the reading of hex address differently. This way looks more complicated, but it works for 7 or 8 bits of address so I didn’t try changing it (I’m running out of time for doing different things!!)

My evaulation 

Direct mapping vs. Fully associative vs. M_Way Set Associative 
Direct mapping is easy to implement with straightforward algorithms of calculating which set address to go into. But it lacks flexibility - if one set address happens to be called multiple times by different memory addresses (that are directed to the same set address), it would be an inefficient cache. So this would waste lots of space (excess blocks) and would waste time writing back and forth between the cache and the main memory. 

Fully associative cache lets you store in any cache block, which means it can be placed in any unused block. This is more space efficient and would eliminate the problem above of frequently accessed addresses coincidentally being directly mapped into the same cache block. Especially, with algorithms last least recently used for evicting the block, you can maintain your cache to be as "productive" as one can intend it to be. However, this is quite expensive to implement because every cache block has to be checked to see if the called block exists in the cache or not.

The happy medium is to use set associativity. By having sets that specific blocks belong to, the programme wouldn't have to search through every single cache block, but just has to iterate through the blocks within the designated set. 

Evicting algorithms
FIFO vs. LRU
I think FIFO is easier to implement. There are pros and cons to using LRU. It is more challenging to implement, but if certain addresses are accessed frequently, it makes more sense to use LRU than FIFO. But if not a lot of the addresses are accessed multiple times, then FIFO would be more efficient because LRU requires iterating through doubly linked list every time you want to rearrange the order. However, for FIFO you just need to keep track of head, tail and append to the end of the list. (You do fewer searches through the list in FIFO). So I guess this would depend on the trace itself. 

Write methods 

Write-alloc vs. No-write-alloc
If the code is store-heavy, I think write-alloc makes more sense. If not, every store has to go through the main memory. Whereas, if it is load-heavy, many of the addresses may already be in the cache, so no-write-alloc may be more beneficial. 

Write-through vs. Write-back
Write through is easier to implement because you just need to update the memory every time something is written into cache. However, it slows down the process because it takes many more processor cycles to transfer memory from/to the main memory to/from the cache. But with write-back, it is more complicated to implement because you have to keep track of the dirty bit. 

I think the best configuration should be write-alloc & write-back. For write-through with write-allocate, you update ht emain memory every time so it is costly in time. For write-through and no-write-alloc, it would be great if addresses that aren't nearby are accessed randomly here and there. However, if one address is excessed, it is often the case that neighbouring addresses are also accessed in the near future. So I think write-alloc and write-back would the be most effective.


Experiments I ran:
SWIM.TRACE
trying out full-associative
./csim 1 8 16 write-allocate write-back lru < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 88064
Load misses: 132604
Store hits: 42943
Store misses: 39582
Total cycles: 63048493

Hit rate = (88064 + 42943) / (220668 + 82525) * 100 = 43.2 %
Miss rate = (132604 + 39582) / (220668 + 82525) * 100 = 56.8 %
Miss penalty = 0.568 * 63048493 = 35,805,800 cycles 
Total cache size = 1 * 8 * 16 + 8 * 1 = 136


Direct-mapped
./csim 8 1 16 write-allocate write-back lru < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 110
Load misses: 220558
Store hits: 14
Store misses: 82511
Total cycles: 129782893

Hit rate = (110 + 14) / (220668 + 82525) * 100 = 96.0 %
Miss rate = (220558 + 82511) / (220668 + 82525) * 100 = 4.0 %
Miss penalty = 0.04 * 129782893 = 535,340 cycles 
Total cache size = 256 * 8 * 16 + 8 * 256 = 34,816

Low hit rate, high miss rate for both direct mapped and full associative, but much lower in direct mapped. 

SET ASSOCIATIVE

./csim 256 8 16 write-allocate write-through fifo < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 219167
Load misses: 1501
Store hits: 71930
Store misses: 10595
Total cycles: 13383498

Hit rate = (219167 + 71930) / (220668 + 82525) * 100 = 96.0 %
Miss rate = (1501 + 10595) / (220668 + 82525) * 100 = 4.0 %
Miss penalty = 0.04 * 13383498 = 535,340 cycles 
Total cache size = 256 * 8 * 16 + 8 * 256 = 34,816

./csim 256 8 16 write-allocate write-through lru < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 219607
Load misses: 1061
Store hits: 72010
Store misses: 10515
Total cycles: 13175578

Hit rate = (219607 + 72010) / (220668 + 82525) * 100 = 96.2%
Miss rate = (1061 + 10515) / (220668 + 82525) * 100 = 3.8%
Miss penalty = 0.038 * 13175578 = 503,047 cycles
Total cache size = 256 * 8 * 16 + 8 * 256 = 34,816

./csim 256 8 16 no-write-allocate write-through fifo < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 218085
Load misses: 2583
Store hits: 58032
Store misses: 24493
Total cycles: 3761200

Hit rate = (218085 + 58032) / (220668 + 82525) * 100 = 91.1%
Miss rate = (2583 + 24493) / (220668 + 82525) * 100 = 8.9%
Miss penalty = 0.089 * 3761200 = 334,747 cycles
Total cache size = 256 * 8 * 16 + 8 * 256 = 34,816

./csim 256 8 16 no-write-allocate write-through lru < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 218123
Load misses: 2545
Store hits: 58046
Store misses: 24479
Total cycles: 3744614

Hit rate = (218123 + 58046) / (220668 + 82525) * 100 = 91.1%
Miss rate = (2545 + 24479) / (220668 + 82525) * 100 = 8.9%
Miss penalty = 0.089 * 3744614 = 333,271 cycles
Total cache size = 256 * 8 * 16 + 8 * 256 = 34,816

./csim 256 8 16 write-allocate write-back fifo < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 219167
Load misses: 1501
Store hits: 71930
Store misses: 10595
Total cycles: 12154893

Hit rate = (219167 + 71930) / (220668 + 82525) * 100 = 96.0%
Miss rate = (1501 + 10595) / (220668 + 82525) * 100 = 4.0%
Miss penalty = 0.04 * 12154893 = 484,924 cycles
Total cache size = 256 * 8 * 16 + 8 * 256 + 256 * 8 = 36,864

./csim 256 8 16 write-allocate write-back lru < swim.trace
Total loads: 220668
Total stores: 82525
Load hits: 219607
Load misses: 1061
Store hits: 72010
Store misses: 10515
Total cycles: 11859293

Hit rate = (219607 + 72010) / (220668 + 82525) * 100 = 96.2%
Miss rate = (1061 + 10515) / (220668 + 82525) * 100 = 3.8%
Miss penalty = 0.038 * 11859293 = 452,791 cycles
Total cache size = 256 * 8 * 16 + 8 * 256 + 256 * 8 = 36,864

(Total cache size is bigger for write-back options because of the dirty bits)

Best Configuration: ./csim 256 8 16 write-allocate write-through lru < swim.trace
with a close second of ./csim 256 8 16 write-allocate write-back lru < swim.trace (my predicted best configuration)

I think the results could be different if I tried with different traces

