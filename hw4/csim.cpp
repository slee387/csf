// Soo Hyun Lee
// slee387
// 601.229
// csim.cpp

#include <iostream>
#include <cstdio>
#include <bitset>
#include <string>
#include <sstream>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <map>
#include <vector>
#include <deque>


using std::cout;
using std::endl;
using std::string;
using std::cerr;

using std::cin;
using std::getline;
using std::map;
using std::stoi;
using std::vector;
using std::deque;
using std::make_pair;
//using std::map::count;
//using std::map::contains;

typedef struct Node {
	Node *next;
	Node *prev;
	string tag;
	bool dirty;
} Node;

typedef std::pair<bool, std::string> pair;
//typedef std::pair<unsigned int, unsigned int> pair2;
typedef std::pair<Node*, Node*> n_pair;


/* helper function to check the validity of argument values */
bool check_arg(int n) {
	if (n < 0) {
		return false;
	}

	while (n > 1) {
		if (n % 2 == 0) {
			n /= 2;
		}
	}

	if (n == 1) {
		return true;
	} else {
		return false;
	}

}

/** finding the x in 2^x value. */
int check_power(int n) {
	int count = 0;
	while (n > 1) {
		if (n % 2 == 0) {
			n /= 2;
			count++;
		}
	}

	return count;
}

/** converting hex address into dec*/
unsigned int hex2dec(string hex) {
	
	unsigned int ret = 0;
	int ascii = 0;
	int len = (int) hex.length();

	for (int i = 0; i < len; i++) {
		
		ret *= 16;
		int check = (int) hex.at(i);

		if (check >= 48 && check <= 57) {
			ascii = check - 48;

		} else if (check >= 65 && check <= 70) {
			ascii = check - 55;

		} else if (check >= 97 && check <= 102) {
			ascii = check - 87;

		} else {
			cerr << "Invalid hex address" << endl;
			return 1;
		}
		ret += ascii;

	}
	return ret;
}

string dec2bin(unsigned int dec) {
	string ret = "";

	while (dec > 0) {
		if (dec % 2 == 1) {
			ret = "1" + ret;
		} else {
			ret = "0" + ret;
		}
		dec /= 2;
	}

	int missing = 32 - ret.length();

	for (int i = 0; i < missing; i++) {
		ret = "0" + ret;
	}
	return ret;
}

int bin2dec(string bin) {
	int ret = 0;
	int len = (int) bin.length();

	for (int i = 0; i < len; i++) {
		ret *= 2;

		int check = (int) bin.at(i);
		if (check % 2 == 1) {
			ret += 1;
		}

	}

	return ret;
}

Node* create_node(string tag) {
	Node* n = new Node;
	n->prev = NULL;
	n->next = NULL;
	n->tag = tag;
	n->dirty = false;

	return n;
}

Node* insert_tail(Node* tail, Node* n) {
	//Node* new_tail = tail;
	n->prev = tail;
	tail->next = n;
	tail = n;
	return tail;
}

Node* remove_head(Node* head) {
	Node* new_head = head->next;
	new_head->prev = NULL;
	head->next = NULL;
	head = new_head;
	return head;
}

void move2end(Node* n, Node*& head, Node*& tail) {
	Node* temp = head;

	while (temp != NULL) {

		if (temp->tag == n->tag) { //FOUND

			if (temp->prev == NULL) { //at the front of list 
				//cout << "front of LL" << endl;
				//Node * head_copy = head;
				head = temp->next;
				//cout << "1" << endl;
				head->prev = NULL;
				//cout << "2" << endl;
				tail->next = n;
				//cout << "3" << endl;
				n->prev = tail;
				//cout << "4" << endl;
				n->next = NULL;
				//cout << "5" << endl;
				tail = n;
				//head = head_copy;

			} else if (temp->next != NULL) { //mid list, if at the end, do nothing
				//cout << "Mid list" << endl;
				temp->prev->next = temp->next;
				temp->next->prev = temp->prev;
				n->prev = tail;
				tail->next = n;
				n->next = NULL;
				tail = n;
				//cout << "Tail here: " << bin2dec(tail->tag) << endl;

			} else {
				//cout << "End of list" << endl;
			}

			//cout << "New head: " << bin2dec(head->tag) << endl;
			//cout << "New tail: " << bin2dec(tail->tag) << endl;

			break;
		} //end of find 

		temp = temp->next;
	}
}


int main(int argc, char *argv[]) {

	if (argc < 6) {
		cerr << "Not enough arguments" << endl;
		return 1;
	}

	bool direct = false;
	bool full = false;
	bool m_way = false;

	// think about when isn't int 
	int sets = stoi(argv[1]);
	if (!check_arg(sets)) {
		cerr << "Need a positive power of 2 for sets" << endl;
		return 1;
	}
	double log = (double) log2(sets);
	int log_sets = (int) log;


	int blocks = stoi(argv[2]);
	if (!check_arg(blocks)) {
		cerr << "Need a positive power of 2 for blocks" << endl;
		return 1;
	}

	//log = (double) log2(blocks);
	//int log_blocks = (int) log;

	int bytes = stoi(argv[3]);
	if (bytes < 4) {
		cerr << "Need at least 4 bytes" << endl;
		return 1;
	}
	log = (double) log2(bytes);
	int log_bytes = (int) log;

	if (!check_arg(bytes)) {
		cerr << "Need a positive power of 2 for bytes" << endl;
		return 1;
	}

	if (blocks == 1) {
		direct = true;
	} else if (sets == 1 && !direct) {
		full = true;
	} else {
		m_way = true;
	}


	string alloc = argv[4];
	bool write_alloc = false;

	if (alloc == "write-allocate") {
		write_alloc = true;
	} else if (alloc == "no-write-allocate") {
		write_alloc = false;
	} else {
		cerr << "Choose between write-allocate or no-write-allocate" << endl;
		return 1;
	}


	string thru = argv[5];
	bool write_thru = false;

	if (thru == "write-through") {
		write_thru = true;
	} else if (thru == "write-back") {
		write_thru = false;
	} else {
		cerr << "Choose between write-through or write-back" << endl;
		return 1;
	}

	if (!write_alloc && !write_thru) {
		cerr << "No-write-allocate & write-back combination not allowed" << endl;
		return 1;
	}

	//if (argc )
	string evict = argv[6];
	bool lru = false;

	if (evict == "lru") {
		lru = true;
	} else if (evict == "fifo") {
		lru = false;
	} else {
		cerr << "Choose between lru or fifo" << endl;
		return 1;
	}

	//are files formatted properly or do i need to check for errors 

	int store = 0;
	int load = 0;
	int cycle = 0;

	int load_hit = 0;
	int load_miss = 0;
	int store_hit = 0;
	int store_miss = 0;

	vector<pair> vec;

	for (string line; getline(cin, line);) {
		bool lda = false;
		if (line.at(0) == 's') {
			store++;
			lda = false;
		} else if (line.at(0) == 'l') {
			load++;
			lda = true;
		} else {
			//
		}

		int start = line.find('x') + 1;
		int first_space = line.find(' ');
		int space = -1;
		int x = 1;
		while (space == -1) {
			char ch = line.at(first_space + x);
			if (ch == ' ') {
				space = first_space + x;
			}
			x++;
		}
		
		string hex = line.substr(start,space - start);
		//cout << "hex: " << hex << endl;
		unsigned int dec = hex2dec(hex);
		//printf("dec: %u\n", dec);
		string bin = dec2bin(dec);

		vec.push_back(std::make_pair(lda, bin));

		//cout << "bin: " << bin << endl;

	}

	bool found = false;
	bool lda = false;
	bool dirty = false;

	if (direct) {

		map<unsigned int, string> d;
		for (std::vector<pair>::iterator it = vec.begin(); it != vec.end(); it++) {

			string full_addr = it->second;
			unsigned int set_addr = bin2dec(full_addr);
			set_addr = (set_addr / bytes) % sets;
			string tag = it->second.substr(0, 32 - log_bytes);
			lda = it->first;

			//determining whether specified cache exists or not
			if (d.empty()) { // if empty
				found = false;
			} else {
				if (d.count(set_addr) > 0) { //if set_addr occupied
					if (d.at(set_addr) == tag) { //check tag 
						found = true;
					} else {
						dirty = true;
					}
				}
			}


			if (lda) { //load case

				if (!found) { //load, not found
					d.insert(std::pair<unsigned int, string>(set_addr, tag));
					cycle += 100 * bytes / 4;

				} 

			} else { //store case

				if (found) { // store, found

					if (write_alloc) {
						cycle += 100;
					}

				} else { //store, !found

					if (write_alloc) { //store, !found, WA
						cycle += 100;
						d.insert(std::pair<unsigned int, string>(set_addr, tag));

						// if (d.count(set_addr) > 0) {
						// 	d.erase(set_addr);
						// }

						if (write_thru) { //store, !found, WA, WT
							cycle += 100 * bytes / 4 - 1;

						} else { //store, !found, WA, WB
							
							if (dirty) {
								cycle += 100 * bytes / 4;
							}

						}
					} else { //store, !found, NWA
						if (write_thru) {
							cycle += 99;
						}

					}
				}

			}

			if (found) {
				if (lda) {
					load_hit++;
					cycle++;
				} else {
					store_hit++;
					cycle++;
				}
			} else {
				if (lda) {
					load_miss++;
					cycle++;
				} else {
					store_miss++;
					cycle++;
				}
			}
			found = false;
			dirty = false;
			lda = false;
		}

	} else if (full) {

		std::map<unsigned int, Node*> f; //int = occurrence#, Node = w/ tag
		Node* head;
		Node* tail;
		//int iter = 0;

		for (std::vector<pair>::iterator it = vec.begin(); it != vec.end(); it++) {

			string tag = it->second.substr(0, 32 - log_bytes);
			unsigned int tag_addr = bin2dec(tag);
			//cout << "tag: " << tag << endl;
			lda = it->first;
			Node* n = create_node(tag);

			// checking if node exists 
			if (f.empty()) {
				found = false;
				//cout << "empty: " << endl;

			} else {

				if (f.count(tag_addr) > 0) {
					if (f.at(tag_addr)->tag == tag) {
						found = true;
					} 
				}

			} // end of checking if node exists 


			if (lda) {

				if (found) { //load, found

					if (lru) { //adjusting linked list order if lru
						//cout << "LRU" << endl;
						//seg fault 
						if ((int) f.size() > 1) {
							move2end(n, head, tail);
						}
						//move2end(n, head, tail);
						//cout << "LRU" << endl;
						//tail = n;
					}

				} else { //load, not found 

					if (f.empty()) { //adding to linked list if empty

						head = n;
						tail = n;

					} else if ((int) f.size() >= blocks) { //if full
						f.erase(bin2dec(head->tag));
						// Node * diffx = head;
						// while (diffx != NULL) {
						// 	cout << "DEBUGx's tag: " << bin2dec(diffx->tag) << endl;
						// 	diffx = diffx->next;
						// }
						head = remove_head(head);
						tail = insert_tail(tail, n);

					} else { //not full, not empty
						
						//insert_tail(tail, n);
						tail = insert_tail(tail, n);

					} // end of checking size to see if full or not 

					f.insert(std::pair<unsigned int, Node*> (tag_addr, n));
					// inserting regardless cuz load
					//f.insert(std::pair<int, Node*> (bl, n));
					
					//cout << "load and not found, adding" << endl;
					cycle += bytes * 100 / 4;
					//bl++;

				} // end of if load, found vs. not found 


			} else { //store

				if (found) { //if found

					if (write_alloc) { // write-alloc option
						cycle += 100;
					} //don't add cycles if no-write-alloc

					if (lru) { //moving to front 
						//seg fault ayyyyy

						if ((int) f.size() > 1) {
							move2end(n, head, tail);

							//head = head->next;
							//tail = n;
							//head = remove_head(head);
						}

					}


				} else { // not found

					if (write_alloc) { // write-alloc option
						cycle += 100;

						if (f.empty()) { //map empty, not full 
							head = n;
							tail = n;

						} else if ((int) f.size() < blocks) {
							//insert_tail(tail, n);
							tail = insert_tail(tail, n);
							//tail = n;
						} else {

						} // end of linked list actions for empty, not full maps

						if (write_thru) { //write-alloc, write-thru

							
							if ((int) f.size() >= blocks) {

								f.erase(bin2dec(head->tag));
								head = remove_head(head);
								//f.erase(bin2dec(head->tag));
								//cout << "is this wrong" << endl;
								//cout << "HEad here? " << bin2dec(head->tag) << endl;
								tail = insert_tail(tail, n);


							} else {
								tail = insert_tail(tail, n);

							}

							cycle += 100 * bytes / 4 - 1;

						} else { //write-alloc, write-back

							n->dirty = true;

							if ((int) f.size() >= blocks) {
								if (head->dirty) {
									cycle += 100 * bytes / 4;
								}

								f.erase(bin2dec(head->tag));
								head = remove_head(head);
								tail = insert_tail(tail, n);
								//tail = n;
							}

						} // end of write-thru check 

						//f.insert(std::pair<int, Node*> (bl, n));
						f.insert(std::pair<unsigned int, Node*> (tag_addr, n));
						//bl++;

					} else { //no-write-alloc: don't add 100 

						if (write_thru) {
							cycle += 99;
						}
					}

				} // end of store found vs. not found


			} //end of deciding action between load / store 

			if (found) {
				if (lda) {
					load_hit++;
					cycle++;
					//cout << "load hit" << endl;
 				} else {
					store_hit++;
					cycle++;
					//cout << "store hit" << endl;
				}
			} else {
				if (lda) {
					load_miss++;
					cycle++;
					//cout << "load miss" << endl;
				} else {
					store_miss++;
					cycle++;
					//cout << "store miss" << endl;
				}
			}


			found = false;
			lda = false;
			dirty = false;

		} //end of for vector it loop 

		free(head);
		free(tail);



	} else if (m_way) {

		Node* head;
		Node* tail;
		int ll_size = 0;
		std::map<unsigned int, n_pair> m;
		//int iter = 0;

		for (std::vector<pair>::iterator it = vec.begin(); it != vec.end(); it++) {

			string partial_addr = it->second.substr(32 - log_sets - log_bytes, log_sets);
			unsigned int set_addr = bin2dec(partial_addr);

			string tag = it->second.substr(0, 32 - log_sets - log_bytes);
			//unsigned int tag_addr = bin2dec(tag);
			lda = it->first;
			Node* n = create_node(tag);
			//cout << "tag_addr: " << bin2dec(tag) << endl;
			//cout << "set_addr: " << set_addr << endl;

			if (m.empty()) {
				found = false;
			} else {

				if (m.count(set_addr) > 0) {

					std::map<unsigned int, n_pair>::iterator it2;
					it2 = m.find(set_addr);

					head = it2->second.first;
					//tail = it2->second.second;

					Node * temp = head;
					while (temp != NULL) {

						if (temp->tag == tag) {
							found = true;
							//cout << "FOUND" << endl;
						}

						if (temp->next == NULL) {
							tail = temp;
						}
						temp = temp->next;
						ll_size++;


					}

					
				}
			}

			if (lda) { //load
				if (found) { //load, found
					
					if (lru) { //lru case

						if (ll_size > 1) {
							move2end(n, head, tail);
							m.erase(set_addr);
							m.insert(make_pair(set_addr, n_pair(head, tail)));
							cycle++;
						}
						//if ((int) m.size())
					}
				} else { //load, not found
					if (ll_size == 0) { //if LL is empty
						head = n;
						tail = n;
						m.insert(make_pair(set_addr, n_pair(head, tail)));

					} else if (ll_size == blocks) {
						head = remove_head(head);
						tail = insert_tail(tail, n);
						m.erase(set_addr);
						m.insert(make_pair(set_addr, n_pair(head, tail)));
						//m.insert(make_pair(set_addr, n_pair(head, tail)));
					} else {
						tail = insert_tail(tail, n);
						m.erase(set_addr);
						m.insert(make_pair(set_addr, n_pair(head, tail)));
						//cout << "hhhh" << endl;
					}
					//m.insert(make_pair(set_addr, n_pair(head, tail)));
					cycle += bytes * 100 / 4;

				} // end of if load, found vs. !found

			} else { //store

				if (found) { //store, found

					if (write_alloc) { //store, found, WA
						cycle += 100;
					}

					if (lru) { //store, found, WA, LRU
						if (ll_size > 1) {
							move2end(n, head, tail);
							//cout << "making sure head:" << bin2dec(head->tag) << endl;
							//cout << "making sure tail:" << bin2dec(tail->tag) << endl;
							m.erase(set_addr);
							m.insert(make_pair(set_addr, n_pair(head, tail)));
							//cout << "post: making sure head:" << bin2dec(head->tag) << endl;
							//cout << "post: making sure tail:" << bin2dec(tail->tag) << endl;
						}

					}
				} else { //store, !found

					if (write_alloc) { // store, !found, WA
						cycle += 100;

						if (ll_size == 0) { //if LL is empty
							head = n;
							tail = n;
							m.insert(make_pair(set_addr, n_pair(head, tail)));

						} else if (ll_size < blocks) {
							tail = insert_tail(tail, n);
							m.erase(set_addr);
							m.insert(make_pair(set_addr, n_pair(head, tail)));
						}

						if (write_thru) { // store, !found, WA, WT

							if (ll_size == blocks) {
								head = remove_head(head);
								tail = insert_tail(tail, n);
								m.erase(set_addr);
								m.insert(make_pair(set_addr, n_pair(head, tail)));
							}
							cycle += 100 * bytes / 4 - 1;

						} else { //store, !found, WA, WB
							n->dirty = true;

							if (ll_size == blocks) {
								if (head->dirty) {
									cycle += 100 * bytes / 4;
								}

								head = remove_head(head);
								tail = insert_tail(tail, n);
								m.erase(set_addr);
								m.insert(make_pair(set_addr, n_pair(head, tail)));
							}
						}

						m.insert(make_pair(set_addr, n_pair(head, tail)));	

					} else { //no WA

						if (write_thru) {
							cycle += 99;
						}

					}

				}

			}

			if (found) {
				if (lda) {
					load_hit++;
					//cout << "load hit" << endl;
					cycle++;
				} else {
					store_hit++;
					//cout << "store hit" << endl;
					cycle++;
				}
			} else {
				if (lda) {
					load_miss++;
					//cout << "load miss" << endl;
					cycle++;
				} else {
					store_miss++;
					//cout << "store miss" << endl;
					cycle++;
				}
			}

			found = false;
			lda = false;
			dirty = false;
			ll_size = 0;

		}

		
		free(head);
		free(tail);


	} else {
		cerr << "Incorrect parameters" << endl;
		return 1;
	}


	//1 cycle per load/store to cache
	//100 cycles per 4 bytes transferred to memory

	cout << "Total loads: " << load << endl;
	cout << "Total stores: " << store << endl;
	cout << "Load hits: " << load_hit << endl;
	cout << "Load misses: " << load_miss << endl;
	cout << "Store hits: " << store_hit << endl;
	cout << "Store misses: " << store_miss << endl;
	cout << "Total cycles: " << cycle << endl;



	return 0;
}