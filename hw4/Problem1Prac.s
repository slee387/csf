# li $t0, 10 # up to
# wait we already have this value in $a3 no need for this

# $t1 = counter for loop
# $t2 = vec1*vec1
# $t3 = vec2*vec2



li $t1, 0 # t1 is our counter 

	la $a0, result
	la $a1, vec1
	la $a2, vec2
	la $t0, n_elts
	lw $a3, 0($t0)

loop: 
	beq $t1, $a3, end
	lw $t2, ($a1)
	mul $t2, $t2, $t2

	lw $t3, ($a2)
	mul $t3, $t3, $t3

	add $t2, $t2, $t3
	sw $a0, $t2

	addi $a1, $a1, 4
	addi $a2, $a2, 4
	addi $t1, $t1, 1
	j loop

end: ## something



lw $t2, 0($a1) # load first array value into t2
mul $t2, $t2, $t2

# or can i do
# mul $t2, 0($a1), 0($a1)

lw $t3, 0($a2)
mul $t3, $t3, $t3
add $t4, $t2, $t3

sw $a0, 0($t4)




lw $a0, ($t1) # print value at the array pointer
addi $t1, $t1, 4 # advance array pointer