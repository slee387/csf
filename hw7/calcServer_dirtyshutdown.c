#include <stdio.h>      /* for snprintf */
#include "csapp.h"
#include "calc.h"
#include <string.h>

#define LINEBUF_SIZE 1024

volatile int shutdown_cmd;
volatile int thread_count;

struct ConnInfo {
	int client_sock;
	struct Calc *calc;
	int server_sock;
	//int *thread_count;
	//int *shut;
	//const char *webroot;
};

void chat_with_client(struct Calc *calc, int infd, int outfd);
void *worker(void *arg);

int main(int argc, char **argv) {

	if (argc < 2) {
		perror("Not enough arguments\n");
		exit(0);
	}
	//char * server_port = argv[1];

	// receive server port and check it is in the right range 
	int server_port_check = atoi(argv[1]);
	if (server_port_check < 1024) {
		perror("Invalid port number, needs to be >= 1024");
		exit(-1);
	}

	int server_sock;
	struct sockaddr_in server_addr;
	unsigned int len;

	//open a server socket 
	server_sock = Socket(AF_INET, SOCK_STREAM, 0);

	//printf("Debug\n");

	//error if unable to create socket 
	if (server_sock == -1 || server_sock == -2) {
		perror("Unable to create socket\n");
		exit(server_sock);
	}

	//configuring the server socket 

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(server_port_check);
	server_addr.sin_addr.s_addr = INADDR_ANY;

	//printf("Debug\n");
	//zeroing out
	bzero(&server_addr.sin_zero, 8);

	//printf("Debug\n");

	len = sizeof(struct sockaddr_in);

	//bind socket
	Bind(server_sock, (struct sockaddr *) &server_addr, len);

	//start a listening stream 
	Listen(server_sock, 5);
	//printf("Debug\n");

	//create calc 
	struct Calc *calc = calc_create();

	int client_sock;
	shutdown_cmd = 0;
	pthread_t thr_id[60];
	thread_count = 0;
	//int shut = 0;

	fd_set set;
	struct timeval timeout;
	int rv;
	FD_ZERO(&set);
	FD_SET(server_sock, &set);

	timeout.tv_sec = 99999999;
	timeout.tv_usec = 0;


	while (1) {
		rv = select(server_sock + 1, &set, NULL, NULL, &timeout);

		if (rv == -1) {
			printf("Shutting down...\n");
			return 0;
		} else if (rv == 0) {
			//perror("timeout ccured in 2 seconds");
			printf("Timeout\n");
			close(server_sock);
			calc_destroy(calc);
			return 0;
		} else {

			client_sock = Accept(server_sock, NULL, NULL);
			//printf("Accepted: %d", client_sock);
			struct ConnInfo *info = malloc(sizeof(struct ConnInfo));
			info->client_sock = client_sock;
			info->calc = calc;
			info->server_sock = *&server_sock;

			if (thread_count <= 50) {
				if (pthread_create(&thr_id[thread_count], NULL, worker, (void *) info) != 0) {
			//if (pthread_create(&thr_id, NULL, worker, info) != 0) {
					perror("pthread_create failed");
					exit(-1);
				}
				thread_count++;
				printf("thread count: %d\n", thread_count);
			} else {
				printf("too many clients\n");
			}
		}
	}

	return 0;


}

void chat_with_client(struct Calc *calc, int infd, int outfd) {
	//bool quit = false;
	rio_t in;
	char linebuf[LINEBUF_SIZE];

	/* wrap standard input (which is file descriptor 0) */
	rio_readinitb(&in, infd);

	/*
	 * Read lines of input, evaluate them as calculator expressions,
	 * and (if evaluation was successful) print the result of each
	 * expression.  Quit when "quit" command is received.
	 */
	int done = 0;
	while (!done) {
		ssize_t n = rio_readlineb(&in, linebuf, LINEBUF_SIZE);
		if (n <= 0) {
			/* error or end of input */
			done = 1;
		} else if (strcmp(linebuf, "quit\n") == 0 || strcmp(linebuf, "quit\r\n") == 0) {
			/* quit command */
			done = 1;
		} else if (strcmp(linebuf, "shutdown\n") == 0 || strcmp(linebuf, "shutdown\r\n") == 0) {
			/* shut down */
			done = 1;
			shutdown_cmd = 1;
			//*shut = 1;
		} else {
			/* process input line */
			int result;
			if (calc_eval(calc, linebuf, &result) == 0) {
				/* expression couldn't be evaluated */
				rio_writen(outfd, "Error\n", 6);
			} else {
				/* output result */
				int len = snprintf(linebuf, LINEBUF_SIZE, "%d\n", result);
				if (len < LINEBUF_SIZE) {
					rio_writen(outfd, linebuf, len);
				}
			}
		}
	}
}

void *worker(void *arg) {
	struct ConnInfo *info = arg;
	// int client_sock = arg;
	

	/*
	 * set thread as detached, so its resources are automatically
	 * reclaimed when it finishes
	 */
	pthread_detach(pthread_self());
	/* handle client request */
	chat_with_client(info->calc, info->client_sock, info->client_sock);
	thread_count--;
	//TO DO::::: pthread_mutex_lock()
	//chat_with_client(info->clientfd, info->webroot);
	//server_chat_with_client(info->clientfd, info->webroot);
	//close(client_sock);
	//free(client_sock);
	close(info->client_sock);
	free(info);

	if (shutdown_cmd == 1) {
		close(info->server_sock);
		calc_destroy(info->calc);
		// shutdown(1,1);
	}


	return NULL;
}
