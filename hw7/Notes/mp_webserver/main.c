/*
 * Very basic webserver
 * Only supports GET requests, only serves files, content types
 * are hard-coded
 *
 * Multi-process concurrent version: client connections are handled by
 * forked child processes
 *
 * main function
 */

#include <stdio.h>
#include "csapp.h"
#include "webserver.h"

/* limit on the maximum number of child processes */
#define MAX_PROCESSES 5

/* current number of child processes running */
int g_num_procs;

/*
 * Handler for SIGCHLD: helps to maintain g_num_procs
 * when the server is not explicitly waiting for
 * a process to finish
 */
void sigchld_handler(int signo) {
	(void)signo;
	/* Wait for a process: should not block */
	int wstatus;
	wait(&wstatus);
	if (WIFEXITED(wstatus) || WIFSIGNALED(wstatus)) {
		g_num_procs--;
	}
}

/*
 * Block or unblock SIGCHLD signal.
 * The how parameter should be SIG_BLOCK or SIG_UNBLOCK.
 */
void toggle_sigchld(int how) {
	sigset_t sigs;
	sigemptyset(&sigs);
	sigaddset(&sigs, SIGCHLD);
	sigprocmask(how, &sigs, NULL);
}

/*
 * Wait until the number of processes running is at least
 * one less than the maximum.
 */
void wait_for_avail_proc(void) {
	/* block SIGCHLD temporarily to avoid race condition */
	toggle_sigchld(SIG_BLOCK);

	/* wait for number of processes running to fall below max */
	while (g_num_procs >= MAX_PROCESSES) {
		/* too many processes running, wait for one to finish */
		int wstatus;
		wait(&wstatus);
		if (WIFEXITED(wstatus) || WIFSIGNALED(wstatus)) {
			/* child proc exited or was terminated by signal */
			g_num_procs--;
		}
	}

	/* reenable SIGCHLD handling */
	toggle_sigchld(SIG_UNBLOCK);
}

int main(int argc, char **argv) {
	if (argc != 3) {
		fatal("Usage: mp_webserver <port> <webroot>");
	}

	const char *port = argv[1];
	const char *webroot = argv[2];

	/* register handler for SIGCHLD, so the server knows when
	 * child processes have terminated */
	struct sigaction sa;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sa.sa_handler = sigchld_handler;
	sigaction(SIGCHLD, &sa, NULL);

	int serverfd = open_listenfd((char*) port);
	if (serverfd < 0) {
		fatal("Couldn't open server socket");
	}

	while (1) {
		/*printf("g_num_procs=%d\n", g_num_procs);*/

		/* wait until fewer than the maximum number of child
		 * processes are running */
		wait_for_avail_proc();

		/* accept a connection
		 * slightly complicated because accept() could be
		 * interrupted by SIGCHLD */
		int clientfd;
		do {
			clientfd = accept(serverfd, NULL, NULL);
		} while (clientfd < 0 && errno == EINTR);
		if (clientfd < 0) {
			fatal("Error accepting client connection");
		}

		/* use a child process to communicate with the client */

		/* start child process!
		 * SIGCHLD is blocked while incrementing g_num_procs
		 * to avoid race condition with signal handler */
		toggle_sigchld(SIG_BLOCK);
		g_num_procs++;
		toggle_sigchld(SIG_UNBLOCK);
		pid_t pid = fork();

		if (pid < 0) {
			fatal("fork failed");
		} else if (pid > 0) {
			/* in parent */
			close(clientfd);
		} else {
			/* in child! */
			server_chat_with_client(clientfd, webroot);
			close(clientfd);
			exit(0);
		}
	}
}
