Notes

Stack Overflow

struct ObjectCustomData {   
  std::string objectType;    
  bool global_lock;   
  std::map<std::string, lawVariables> lawData;   
 };


void test(ObjectCustomData& data)
{
  lawVariables& variable = data.lawData["law_1"];
  variable["var_a"] = 39.3;

}

 ObjectCustomData data;
  data.lawData["law_1"]["var_a"] = 39.3;
  data.lawData["law_1"]["var_g"] = 8.1;
  data.lawData["law_8"]["var_r"] = 83.1;

  if (!data.lawData.count("law_x")) {
    cout<<"law_x not found"<<endl;
  }

  if (data.lawData.count("law_1")) {
    cout<<"law_1 was found"<<endl;
  }


initialising using an initialiser list 

  std::map<std::string, int> mapOfMarks = {
		{"Riti",2},
		{"Jack",4}
};


If mine is:
	struct CalcImpl {
		map<string, int> m;
	};

can i do:

	CalcImpl(): m = 



	https://www.geeksforgeeks.org/when-do-we-use-initializer-list-in-c/


	Wait non-static or static? figure this out
	where to add private / public 


	class myClass
{
public:
   // default member initializations
   int val = { };         // zero-initialization
   foo bar = { 0, 0.0f }; // aggregate-initializing foo here, just giving { } will zero all of myClass::bar

   // should you want to receive an element from the constructor, this can be done too
   // aggregate initializing a struct in constructor initialization list is allowed from C++11 onwards
   // in C++03, we would've resorted to just setting the member of bar inside the constructor body
   myClass(int _foo1) : bar{_foo1, 0.f}, val{}
   {
   }

   // since we've a non-default constructor, we've to re-introduce the default constructor
   // if we need the above in-class initialization to work
   myClass() = default;
};

struct MyLocalData
{
    Data1 value1;
    Data2 value2;
    Data3 value3;
 
    MyLocalData(BigObject const& bigObject, LargeAPI const& largeAPI)
    : value1(getValue1(bigObject)
    , value2(getValue2(bigObject, largeAPI)
    , value3(getValue3(largeAPI))
    {}
};


Can I do like

struct CalcImpl {
	
	map<string, int> m;

};

CalcImpl(): m{"X", 0} {}
CalcImpl(): m() {}


https://stackoverflow.com/questions/4344464/initializing-map-and-set-class-member-variables-to-empty-in-c

Class with:
std::map<int, Node*> a;
std::set<Node*> b;

How can these member variables a and b be initialized to empty in the constructor of the class they are in?

class A
{
  public :

  A() : s(),
        m()
  {
  }


  std::set< int > s;
  std::map< int, double > m;
};


SomeClass() : a(), b() {}



