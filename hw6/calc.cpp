#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <sstream>



using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::stringstream;
using std::stoi;
using std::map;
using std::pair;
using std::replace;
// using std::make_pair;

//typedef pair <string, int> pair;

struct Calc {
};

class CalcImpl : public Calc {
	
	// protected:
	// struct Calc {

	// 	map<string, int> m;
	// 	Calc(){};
	// 	//Calc(): m() {}
	// };
protected:
	map<string, int> m;
	// CalcImpl(): m() {}

	//Calc(): m() {}

public:
	CalcImpl(): m() {}

	// Calc(): m() {}

	// protected:
	// 	map<string, int> m;


	// struct CalcImpl {
	// 	map<string, int> m;
	// };
	// public:
	// 	map<string, int> m;
		// string operand1;
		// char op;
		// string operand2;
	//CalcImpl(): m() {}
	//vector<string> tokenize(const string &expr);
private:
	//vector<string> tokenize(string expr) {
	vector<string> tokenize(const string &expr) {
	    vector<string> vec;
	    stringstream s(expr);
	    //size_t pos = -1;

	    string tok;
	    while (s >> tok) {

 	        vec.push_back(tok);
	    }
	   
		// for (int i = 0; i < (int) vec.size(); i++) {
		// 	cout << "Vector print: " << vec[i] << endl;
		// }

	    return vec;
	}

	bool is_variable(string s) {

		int len = (int) s.length();

		for (int i = 0; i < len; i++) {

			char c = s.at(i);
			if (!('a' <= c && c <= 'z')) {
				if (!('A' <= c && c <= 'Z')) {
					return false;
				}
			}
		}

		return true;
	}

	bool is_num(string s) {
		//cout << "S received @is_num: " << s << endl;
		int len = (int) s.length();

		char c = s.at(0);

		if (c != '+' && c != '-') {
			if (c < '0' || c > '9') {
				return false;
			}
		}

		for (int i = 1; i < len; i++) {
			c = s.at(i);
			if (c < '0' || c > '9') {
				//cout << "0 - 9" << endl;
				return false;
			}

		}

		return true;

	}

	bool is_operand(string s) {
		int len = s.length();

		if (len != 1) {
			return false;
		}

		if (s == "+" || s == "-" || s == "/" || s == "*") {
			return true;
		} 

		return false;
	}


	int case1(string s, int &result) {

					// cout << "RESULT: " << result << endl;
			// cout << "Case 5: ret" << ret << endl;
			// cout << "m[" << var << "]\n";
			// cout << "m[var]" << m[var] << endl;
			// m[var] = result;
		//variable name input, print var
		if (is_variable(s)) {
			//cout << "true" << endl;
			if (m.empty()) {
				result = 0;
				return 0;
			}
			//typename std::map<string, int>iterator:: it = m.find(s);
			//map<string, int>iterator:: it = m.find(s);
			if (m.find(s) != m.end()) {
				result = m[s];
				return 1;
			} else {
				result = 0;
				return 0;
			}

			//1 if found, else 0
			return 1;

		// one number input, print number
		} else if (is_num(s)) {
			
			result = stoi(s);
			return 1;

		//neither
		} else {
			result = 0;
			return 0;
		}

	}

	int case3op(string s1, string op, string s2, int &result) {

		bool op1_isNum = false;
		bool op2_isNum = false;
		int first = 0;
		int second = 0;


		// cout << "Is num?: " << is_num(s1) << endl;
		// cout << "Is var?: " << is_variable(s1) << endl;
		// cout << "Result " << result;
		// cout << "When" << endl;

		if (is_num(s1)) {
			op1_isNum = true;
			//cout << "Are we here? " << endl;
			first = stoi(s1);
		} else if (is_variable(s1)) {
			//cout << "When" << endl;
			op1_isNum = false;
			
			if (m.empty()) {
				result = 0;
				return 0;
			}
		} else {
			result = 0;
			return 0;
		}

		if (is_num(s2)) {
			op2_isNum = true;
			second = stoi(s2);
		} else if (is_variable(s2)) {
			op2_isNum = false;

			if (m.empty()) {
				result = 0;
				return 0;
			}
		} else {
			result = 0;
			return 0;
		}

		
		//if first variable is a var
		if (!op1_isNum) {

			if (m.find(s1) != m.end()) {
				//cout << "When" << endl;
				first = m[s1];
			} else {
				result = 0;
				return 0;
			}
		}


		//if second variable is a var
		if (!op2_isNum) {

			if (m.find(s2) != m.end()) {
				second = m[s2];
			} else {
				result = 0;
				return 0;
			}
		}


		if (op == "+") {
			result = first + second;
		} else if (op == "-") {
			result = first - second;
		} else if (op == "*") {
			result = first * second;
		} else if (op == "/") {
			if (second == 0) {
				result = 0;
				return 0;
			}
			result = first / second;
		} else {
			result = 0;
			return 0;
		}

		return 1;

	}

	int case3var(string var, string s, int &result) {

		//case: var = var
		if (is_variable(s)) {
			if (m.empty()) {
				result = 0;
				return 0;
			}

			if (m.find(s) != m.end()) {
				int save = m[s];
				m[var] = save;
				result = save;
				return 1;
			} else {
				result = 0;
				return 0;
			}
			

		//case: var = int value 
		} else {
			//cout << "IM HERE" << endl;
			int convert = stoi(s);
			m[var] = convert;
			result = convert;
			return 1;

			//cout << "CONVERT: " << convert << endl;

			if (m.empty()) {
				m[var] = convert;
			} else {
				if (m.find(var) != m.end()) {
					m[var] = convert;
				} else {
					m[var] = convert;
				}
			}

		}
		return 0;
	}

	int case5(string var, string s1, string op, string s2, int &result) {

		int ret = -1;

		if (is_variable(var)) {
			// cout << "Var: " << var << endl;
			// cout << "s1: " << s1 << endl;
			// cout << "s2:: " << s2 << endl;

			ret = case3op(s1, op, s2, result);

			if (ret == 0) {
				result = 0;
				return 0;
			}
			// cout << "RESULT: " << result << endl;
			// cout << "Case 5: ret" << ret << endl;
			// cout << "m[" << var << "]\n";
			// cout << "m[var]" << m[var] << endl;
			m[var] = result;
			return 1;
		} else {
			result = 0;
			return 0;
		}

		return 0;
	}


public:
	int evalExpr(const char * expr, int &result) {


		string send = expr;
		int ascii = (int) send.back();
		//handling \n and \r
		if (ascii == 10 || ascii == 13) {
			send.pop_back();
		}
		vector<string> checked = tokenize(send);
		int len = checked.size();

		//case1: var or op --> just save value in result
		if (len == 1) {
			int ret = case1(checked[0], result);
			return ret;

		//case3: var = operand OR operand op operand
		} else if (len == 3) {
			//var = operand 
			if (checked[1] == "=") {
				//is the 'var' a valid svariable name
				if (is_variable(checked[0])) {
					//cout << "checked[0]: " << checked[0] << endl;
					//cout << "checked[2]: " << checked[2] << endl;

					int ret = case3var(checked[0], checked[2], result);
					// cout << "Result: " << result << endl;
					// cout << "RET: " << ret << endl;
					return ret;

				} else {

					result = 0;
					return 0;
				}

			// operand op operand 
			} else if (is_operand(checked[1])) {
				int ret = case3op(checked[0], checked[1], checked[2], result);
				return ret;

			// neither --> wrong 
			} else {
				result = 0;
				return 0;
			}

		//case5: var = operand op operand
		} else if (len == 5) {
			if (checked[1] != "=") {
				result = 0;
				return 0;
			} 

			if (!is_operand(checked[3])) {
				result = 0;
				return 0;
			}

			int ret = case5(checked[0], checked[2], checked[3], checked[4], result);
			return ret;

		//fits none? 0
		} else {

			result = 0;
			return 0;
		}


		return 0;
	}

};

	//or use getters, setters for this?
extern "C" struct Calc *calc_create(void) {
	//return;
	return new CalcImpl();
}

extern "C" void calc_destroy(struct Calc *calc) {
   	CalcImpl *obj = static_cast<CalcImpl *>(calc);
   	delete obj;
}

extern "C" int calc_eval(struct Calc *calc, const char *expr, int *result) {
	CalcImpl *obj = static_cast<CalcImpl *>(calc);
	return obj->evalExpr(expr, *result);
}

