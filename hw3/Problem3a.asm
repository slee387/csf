; Soo Hyun Lee
; slee387@jhu.edu

entry:
    LDA #$05
    TAX
    JSR popcount
    BRK

popcount:
    TXA            ; put arg in accumulator
    LDX #$08       ; loading 8 into X (will decrement from 8)
    LDY #$00       ; loading 0 into Y (bit counter)

loop:   
    ASL A          ; shift to check if MSB is 1
    BCS icr        ; if carry == 1, jump to icr

here:
    CLC
    DEX            ; decrease X
    BNE loop       ; repeat loop if zero flag is clear
    JMP done       ; jump to RTS when done

icr:
    INY            ; increase y
    JMP here       ; jump back to the loop after this

done:
    RTS            ; return to retrieved address