; Soo Hyun Lee
; slee387@jhu.edu


	LDA #$08    ; shift right by 2 bits
	TAX         ; put the shift amount in the X register
	LDA #$5c    ; put the data value $5C in the accumulator
	JSR right_shift_n
	BRK
	; at this point, A will contain the value $17

right_shift_n:
	LSR A
	CLC
	DEX
	BNE right_shift_n
	RTS