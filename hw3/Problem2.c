/*
 * CSF Fall 2019
 * Homework 3
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>

#define SIZE 16

// Method that prints the 3-letter instruction code
void print_inst(int x);

int main() {

	unsigned char buf[SIZE] = {0};
	int c;
	int a = 0;

    // Reading stdin into buffer
    while ((c = fgetc(stdin)) != EOF) {
        buf[a] = c;
        a++;
    }

    // if buffer exceeds 16 bytes, print error
    if (a >= 16) {
        fprintf(stderr, "too long\n");
    }


    for (int i = 0; i < a; i++) {
    	// Printing address: i into hex
        printf("%01x: ", i);
        // Instruction reads the first number of the two in hex
        print_inst(buf[i] / 16);
        // Remainder to extract the second digit in the hex
        printf("%01x\n", buf[i] % 16);
    }

	return 0;
}

void print_inst(int x) {

	switch(x) {
		case 0:
			printf("%s", "HLT ");
			break;
		case 1:
			printf("%s", "LDA ");
			break;

		case 2:
			printf("%s", "LDI ");
			break;

		case 3:
			printf("%s", "STA ");
			break;
			
		case 4:
			printf("%s", "STI ");
			break;

		case 5:
			printf("%s", "ADD ");
			break;
			
		case 6:
			printf("%s", "SUB ");
			break;

		case 7:
			printf("%s", "JMP ");
			break;
			
		case 8:
			printf("%s", "JMZ ");
			break;
			
		default:
			printf("%s", "??? ");
			break;
	}


}
