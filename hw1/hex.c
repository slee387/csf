/*
 * CSF Fall 2019
 * Homework 1
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/* The number of data bytes corresponding to one line of hexdump output. */
#define HEXDUMP_BUFSIZE 16
#define SIZE 2000000


int main(int argc, char **argv) {
    
    if (argc < 1) {
        fprintf(stderr, "Not enough arguments");
        exit(1);
    } 

    // Variables for fgetc
    int c;
    int loop = 0;
    char buf[20];
    int count = 0;
    int start = 0;

    // Case 1: stdin
    if (argc == 1) {
        
        // Casting to solve the ff.... issue
        c = (unsigned int) fgetc(stdin);

        while (c != EOF) {

            // Reached the end of line (printing out the 16th char + buffer)
            if (loop % 16 == 15) {
        
                printf("%02x  ", c);

                // If nonprintable/extendable char --> save to buffer as .
                if (!isprint(c)) {
                    buf[count] = '.';
                } else {
                    buf[count] = c;
                }

                // Print out contents of the buffer
                for (int i = 0; i < 16; i++) {
                    printf("%c", buf[i]);
                }

                // Back to position 0 for buffer
                count = 0;

            } else {

                // If starting a new line, add line number
                if (loop % 16 == 0) {

                    // Except for the very first line, need to add \n
                    if (loop == 0) {
                        printf("%08x: ", start);
                    } else {
                        printf("\n%08x: ", start);
                    }

                    start += HEXDUMP_BUFSIZE;
                }

                // Then proceed to print out hex values as usual
                printf("%02x ", c);

                // If char isn't printable, add . to buffer
                if (!isprint(c)) {
                    buf[count] = '.';
                } else {
                    buf[count] = c;
                }

                count++;

            }

            loop++;
            c = (unsigned int) fgetc(stdin);

        }

        // To count how many additional spaces to add
        int missing = 16 - count;

        // Except for the case of empty input, need a new line at the end
        if (count == 0 && loop != 0) {
            printf("%s", "\n");
        } 

        // No need for extra spaces if all 16 chars are filled up
        if (count != 0) {
            for (int j = 0; j < missing * 3 + 1; j++) {
                printf("%s", " ");
            } 

        }

        // Print out buffer if there is anything to print out
        for (int j = 0; j < count; j++) {
            if (!isprint(buf[j])) {
                printf("%s", ".");
            } else {
                printf("%c", buf[j]);
            }

            if (j == count - 1) {
                printf("%s", "\n");
            }
        }

    } else {

        // Case of accepting files 
        // The comments above would apply to the following too
        FILE* fp;
        fp = fopen(argv[1], "r");

        if (fp == NULL) {
            fprintf(stderr, "Can't open the file\n");
            exit(1);
        }

        c = (unsigned int) fgetc(fp);

        while (c != EOF) {

            if (loop % 16 == 15) {

                printf("%02x  ", c);

                if (!isprint(c)) {
                    buf[count] = '.';
                } else {
                    buf[count] = c;
                }

                for (int i = 0; i < 16; i++) {
                    printf("%c", buf[i]);
                }

                count = 0;

            } else {

                if (loop % 16 == 0) {

                    if (loop == 0) {
                        printf("%08x: ", start);
                    } else {
                        printf("\n%08x: ", start);
                    }

                    start += HEXDUMP_BUFSIZE;
                }

                printf("%02x ", c);

                if (!isprint(c)) {
                    buf[count] = '.';
                } else {
                    buf[count] = c;
                }

                count++;

            }
            loop++;

            c = (unsigned int) fgetc(fp);
        }

        int missing = 16 - count;

        if (count == 0 && loop != 0) {
            printf("%s", "\n");
        } 

        if (count != 0) {
            for (int j = 0; j < missing * 3 + 1; j++) {
                printf("%s", " ");
            } 

        }

        for (int j = 0; j < count; j++) {
            if (!isprint(buf[j])) {
                printf("%s", ".");
            } else {
                printf("%c", buf[j]);
            }

            if (j == count - 1) {
                printf("%s", "\n");
            }
        }
    }

    return 0;
}
