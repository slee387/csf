/*
 * CSF Fall 2019
 * Homework 1
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/* The number of data bytes corresponding to one line of hexdump output. */
#define HEXDUMP_BUFSIZE 16

/* TODO: function prototypes */

void convert(char* input, char* output);

//int main(int argc, char **argv) {

int main() {
    // if (argc < 2) {
    //     printf("Not enough input\n");
    //     return 1;
    // }

    int x = getchar();
    char inputRead[10000];
    int count = 0;

    while (x != EOF) {
        inputRead[count] = x;
        ++count;
        x = getchar();
    }

    if (count == 0) {
        fprintf( stderr, "Cannot read file!" );
        exit(1);
    }

    int len = strlen(inputRead);
    char hex_str[(len / HEXDUMP_BUFSIZE + 1) * 76];

    convert(inputRead, hex_str);

    printf("%s", hex_str);

    return 0;
}

void convert(char* input, char* output) {
    
    int loop = 0;
    int i = 0;
    int start = 0;

    int substr = 0;
    int line = 0;

    int len = strlen(input);

    while (input[loop] != '\0') {

        if (loop == 0) {
            sprintf((char*) (output + i), "%08x: ", start);
            start+=HEXDUMP_BUFSIZE;
            i+=10;
            sprintf((char*) (output + i), "%02x ", input[loop]);
            i+=3;
            //printf("1: %s", output);
        } else if (loop % 16 == 0 && loop != 0) {

                sprintf((char*) (output + i), "\n%08x: ", start);
                start+=16;
                i+=11;

                sprintf((char*) (output + i), "%02x ", input[loop]);
                i+=3;

        } else if (loop % 16 == 15){

            sprintf((char*) (output + i), "%02x ", input[loop]);
            i+=3;

            sprintf((char*) (output + i), " ");
            i++;
            for (int b = substr; b < 16 + substr; b++) {
                
                int asc_val = (int) input[b];
                if (asc_val >= 0 && asc_val <= 31) {
                    input[b] = '.';
                }
                if (input[b] == '\n' || input[b] == '\t') {
                    input[b] = '.';
                    //sprintf((char*) (output + i), "%c", input[b]);
                } 
                sprintf((char*) (output + i), "%c", input[b]);

                i++;
            }
            substr+=16;
            line++;

        } else {
            //printf("loop here: %d\n", loop);
            // sub[subC] = input[loop];
            // subC++;
            sprintf((char*) (output + i), "%02x ", input[loop]);
            i+=3;

            //printf("4: %s", output);
        }
        //sprintf((char*) (output + i), "%02x ", input[loop]);
        loop+=1;
        //i+=3;

    }
    // int temp = loop;
    // printf("temp %d\n", temp);

    //len % 16
    //printf("len: %d, rem: %d\n", len, 16 - len % 16);
    if (len != 16 && len != 0) {
        for (int j = i; j < i + 1 + (16 - len % 16)*3; j++) {
            output[j] = ' ';
        //printf("Times\n");
            }
        i+=(16 - len % 16) * 3 + 1;
    }

    for (int k = i; k < i + len % 16; k++) {
        //printf("CHAR: %c\n", input[substr]);
        //printf("S: %s\n", input);
        if (input[substr] == '\n' || input[substr] == '\t') {
            input[substr] = '.';
            //printf("Do we even reach here???");
            //sprintf((char*) (output + k), "%c", '.');
        } 
        sprintf((char*) (output + k), "%c", input[substr]);
        //output[k] = input[substr];
        substr++;

        if (k == i + len % 16 - 1) {
            sprintf((char*) (output + k + 1), "%c", '\n');
        }
    }
    i+=(len % 16) + 2;
    output[i++] = '\0';

    //return 0;
}
