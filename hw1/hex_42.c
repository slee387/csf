/*
 * CSF Fall 2019
 * Homework 1
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/* The number of data bytes corresponding to one line of hexdump output. */
#define HEXDUMP_BUFSIZE 16
#define SIZE 1948576

/* A helper function that receives input and creates a formatted string. */
void convert(char* input, char* output);


int main(int argc, char **argv) {

    // You need at least one argument 
    if (argc < 1) {
        fprintf(stderr, "Not enough arguments");
        exit(1);
    } 

    // Variables for fgetc
    int c;
    char inputRead[SIZE];    
    int pos = 0;
    char hex_str[SIZE * 3];

    // When argc == 1, we read from stdin
    if (argc == 1) {
        c = fgetc(stdin);

        while (c != EOF) {
            inputRead[pos] = c;
            pos++;
            c = fgetc(stdin);
        }

    // When arg > 1, we read inside the text    
    } else {

        FILE* fp;
        fp = fopen(argv[1], "r");

        if (fp == NULL) {
            fprintf(stderr, "Can't open the file\n");
            exit(1);
        }

        c = fgetc(fp);

        while (c != EOF) {
            inputRead[pos] = c;
            pos++;
            c = fgetc(fp);
        }
        fclose(fp);
    }

    convert(inputRead, hex_str);
    printf("%s", hex_str);

    return 0;
}

void convert(char* input, char* output) {
    
    int loop = 0;
    int i = 0;
    int start = 0;

    int substr = 0;
    //int line = 0;

    int len = strlen(input);

    while (input[loop] != '\0') {

        // We are at the last character and we now need to add in the string part

        if (loop % 16 == 15) {
            sprintf((char*) (output + i), "%02x ", input[loop]);
            i+=3;

            sprintf((char*) (output + i), " ");
            i++;
            for (int b = substr; b < 16 + substr; b++) {
                
                // int asc_val = (int) ((unsigned char) input[b]);
                // if ((asc_val >= 0 && asc_val <= 31) || asc_val >= 128) {
                //     input[b] = '.';
                // }

                if (!isprint((unsigned char)input[b])) {
                    input[b] = '.';
                }

                sprintf((char*) (output + i), "%c", input[b]);

                i++;
            }
            substr+=16;
            //line++;

        } else {
            // Went through 16 characters, need a new line
            if (loop % 16 == 0) {
                // If it is the very first character from input, 
                // we don't need a new line
                if (loop == 0) {
                    sprintf((char*) (output + i), "%08x: ", start);
                    i += 10;
                } else {
                    sprintf((char*) (output + i), "\n%08x: ", start);
                    i += 11;
                }

                start += HEXDUMP_BUFSIZE;
            }
            sprintf((char*) (output + i), "%02x ", input[loop]);
            i += 3;
        }

        loop++;

    }

    //If len != multiple of 16, add in spaces (leftover string len * 3)
    int missing = 16 - len % 16;

    if (len % 16 != 0) {
        for (int j = i; j < i + 1 + missing * 3; j++) {
            output[j] = ' ';
        }

        i += missing * 3 + 1;
    } else if (len != 0){
        output[i] = '\n';
        i++;
    } else {
        //
    }

    for (int k = i; k < i + len % 16; k++) {


        // int asc_val = (int) ((unsigned char) input[substr]);
        // if ((asc_val >= 0 && asc_val <= 31) || asc_val >= 128) {
        //     input[substr] = '.';
        // }

        if (!isprint((unsigned char)input[substr])) {
            input[substr] = '.';
        }
        sprintf((char*) (output + k), "%c", input[substr]);
        //output[k] = input[substr];
        substr++;

        if (k == i + len % 16 - 1) {
            sprintf((char*) (output + k + 1), "%c", '\n');
        }
    }
    i+=(len % 16) + 2;
    output[i++] = '\0';
}
