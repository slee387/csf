/*
 * CSF Fall 2019
 * Homework 1
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>
#include <ctype.h>

/* The number of data bytes corresponding to one line of hexdump output. */
#define HEXDUMP_BUFSIZE 16

/* TODO: function prototypes */

void string2hexString(char* input, char* output);

int main(int argc, char **argv) {

    char ascii_str[] = "Hello asdf asdkfjasdf \n";
    int len = strlen(ascii_str);
    char hex_str[(len*3) + 1];

    string2hexString(ascii_str, hex_str);

    printf("%s \n", hex_str);

    return 0;
}

void string2hexString(char* input, char* output) {
    int loop = 0;
    int i = 0;

    //Before entering the loop through the input, 
    // create an int position value and set it equal to 0. 
    //For each iteration of the loop, increment the 
    //position value by 16 and print the hex value of 
    //that number, by using printf format:  
    //printf(“%08x ”, position) which will add 8 leading zeros in front.


    while (input[loop] != '\0') {
        //if (i % 16 == 0)
        //sprintf((char*) output[i + 1], "%02x ", input[loop]);
        // int a, *p = &a;
        // printf("%p ", (void*) p);

        // int start = 0;
        // printf("here: %08X ", start);

        //printf("%08d : ", start);
        // if (loop == 0) {
        //     printf("000000: ");//uhh

        //start += 16;
            
        // }

        if (loop % 16 == 0 && loop != 0) {
           // if (loop != 0) {
            //printf("HERE\n\n");
            //printf("loop: %d\n", loop);
            int start = 16;
            printf("line: %08X : ", start);
            start+=16;

            //printf("%08d : ", start);
            //what is this exactly 

                sprintf((char*) (output + i), "\n%02x ", input[loop]);
                i+=4;
                //start += 10;
           // }
        } else {
            //printf("loop here: %d\n", loop);
            sprintf((char*) (output + i), "%02x ", input[loop]);
            i+=3;
        }
        //sprintf((char*) (output + i), "%02x ", input[loop]);
        loop+=1;
        //i+=3;

    }
    output[i++] = '\0';
}
