/*
 * CSF Fall 2019
 * Homework 1
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>
#include <ctype.h>

/* The number of data bytes corresponding to one line of hexdump output. */
#define HEXDUMP_BUFSIZE 16

/* TODO: function prototypes */

int main(int argc, char **argv) {
    // FILE *in;

    // if (argc > 2) {
    //     fprintf(stderr, "Usage: %s [<filename>]\n", argv[0]);
    //     fprintf(stderr, "Reads from standard input if <filename> is omitted\n");
    //     return 1;
    // }

    // if (argc == 1) {
    //     in = stdin;
    // } else {
    //     in = fopen(argv[1], "rb");
    //     /* TODO: report error and exit if input file couldn't be opened */
    // }

    // char buf[HEXDUMP_BUFSIZE]; /* storage for data read from input */
    // /*
    //  * TODO: read data from the file handle called "in" and print a hexdump
    //  * of its contents
    //  */

    //Let me try just stdin
    const int max = 1024; //wait idk
    int x = getchar();
    char input[max];
    input[max - 1] = '\0';
    int count = 0;

    while (x != EOF) {
        input[count] = x;
        ++count;
        x = getchar();
    }

    //now we have a char array of stdin
    int ascii[max];


    //int i;
    for (int i = 0; i < max; i++) {
        //printf("%c ", input[i]);
        ascii[i] = (int) input[i];
        //printf("%d ", ascii[i]);
        printf("%02x ", ascii[i]);
    }

    return 0;
}
