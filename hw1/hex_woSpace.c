/*
 * CSF Fall 2019
 * Homework 1
 * Soo Hyun Lee
 * slee387@jhu.edu
 */

#include <stdio.h>
#include <ctype.h>

#include <string.h>

/* The number of data bytes corresponding to one line of hexdump output. */
#define HEXDUMP_BUFSIZE 16

/* TODO: function prototypes */

void string2hexString(char* input, char* output);

int main(int argc, char **argv) {

    // if (argc < 2) {
    //     printf("Not enough input\n");
    //     return 1;
    // }

    //char ascii_str[] = "This is a longer example of a hexdump. Marvel at its magnificence.";

    int x = getchar();
    char inputRead[10000];
    int count = 0;

    while (x != EOF) {
        inputRead[count] = x;
        ++count;
        x = getchar();
    }

    int len = strlen(inputRead);
    char hex_str[(len / 16 + 1) * 76];

    string2hexString(inputRead, hex_str);

    //string2hexString(ascii_str, hex_str);

    printf("%s", hex_str);

    return 0;
}

void string2hexString(char* input, char* output) {
    int loop = 0;
    int i = 0;
    int start = 0;

    int substr = 0;
    int line = 0;

    int len = strlen(input);

    while (input[loop] != '\0') {

        if (loop == 0) {
            sprintf((char*) (output + i), "%08x: ", start);
            start+=16;
            i+=10;
            sprintf((char*) (output + i), "%02x", input[loop]);
            i+=2;
            //printf("1: %s", output);
        } else if (loop % 16 == 0 && loop != 0) {
           // if (loop != 0) {
            //printf("HERE\n\n");
            //printf("loop: %d\n", loop);

            //printf("%08d : ", start);
            //what is this exactly 
                sprintf((char*) (output + i), "\n%08x: ", start);
                start+=16;
                i+=11;

                sprintf((char*) (output + i), "%02x", input[loop]);
                i+=2;

                //printf("2: %s", output);

                //start += 10;
           // }
        } else if (loop % 16 == 15){

            sprintf((char*) (output + i), "%02x", input[loop]);
            i+=2;

            sprintf((char*) (output + i), "  ");
            i+=2;
            for (int b = substr; b < 16 + substr; b++) {
                
                if (input[b] == '\n' || input[b] == '\t') {
                    input[b] = '.';
                    //sprintf((char*) (output + i), "%c", input[b]);
                } 
                sprintf((char*) (output + i), "%c", input[b]);

                i++;
            }
            substr+=16;
            line++;

        } else {
            //printf("loop here: %d\n", loop);
            // sub[subC] = input[loop];
            // subC++;
            sprintf((char*) (output + i), "%02x", input[loop]);
            i+=2;

            //printf("4: %s", output);
        }
        //sprintf((char*) (output + i), "%02x ", input[loop]);
        loop+=1;
        //i+=3;

    }

    //len % 16
    //printf("len: %d, rem: %d\n", len, 16 - len % 16);

    for (int j = i; j < i + 2 + (16 - len % 16)*2; j++) {
        output[j] = ' ';
        //printf("Times\n");
    }
    i+=(16 - len % 16)*2 + 2;

    for (int k = i; k < i + len % 16; k++) {
        //printf("CHAR: %c\n", input[substr]);
        //printf("S: %s\n", input);
        if (input[substr] == '\n' || input[substr] == '\t') {
            input[substr] = '.';
            //printf("Do we even reach here???");
            //sprintf((char*) (output + k), "%c", '.');
        } 
        sprintf((char*) (output + k), "%c", input[substr]);
        //output[k] = input[substr];
        substr++;

        if (k == i + len % 16 - 1) {
            sprintf((char*) (output + k + 1), "%c", '\n');
        }
    }
    i+=(len % 16) + 2;
    //sprintf((char*) (output + i), "%c", '\n');
    //i++;
    
    //THIS PART 
    // output[i] = '.';
    

    //printf("output[i]: %c%c%c\n", output[i-3], output[i-2], output[i-1]);

    // TRYING TO ADD SPACES BEFORE GETTING FINAL PART OF STRING DOWN

    //printf("i: %d, loop: %d, substr: %d\n", i, loop, substr);
    //output[i] = '\n';
    output[i++] = '\0';
}
