;    Program to convert an ascii character into  
;    hex digits.
     
          include   equal-01.inc
          
$AsciiToHex  macro   asciiChar, hexStr2
          local  tableHex
; Converts an ASCII character into an 2-character hex String
          .data
tableHex  db   '0123456789ABCDEF'       ; hext table to be used with 
          .code                              ; the xlat instruction
          mov   bx, offset  tableHex    ; address of translate table
          mov   al, [asciiChar]         ; get hex digit    
          mov   ah, al                  ; make copy of hex digit
          and   al, 00001111b           ; isolate rightmost hex digit
          xlat                          ; translate hex digit into hex str
          mov   [hexStr2 + 1], al       ; move hex str into output string 
          shr   ax, 12                  ; isolate leftmost digit
          xlat                          ; translate hex digit into hex str
          mov   hexStr2, al             ; store leftmost digit in str          
          EndM


          model     small     ; program model
          stack     100h      ; stack size
;         To get the 386 or 486 instructions, an assembler directive
;         is used.  It should be placed just after the 
;         stack statement
          
;         .486      ; MASM     
          P486N     ; TASM    

          
          .data                ; data segment

printStr  db   8 dup (' '), $CR, $LF, $StrEnd
asciiChar db   0
hexStr2   db   2 dup ('//')
newLine   db   $CR, $LF, $StrEnd

          .code                   ; code segment
Main      proc                    ; start of procedure
Start:                            ; execution starts here
          mov  ax, @data          ; load data segment address
          mov  ds, ax             ; ds  -> data segment
                    
          mov  ah, $DOSRdChrE      ; DOS read character function
          int  $DOSIFH             ; read character with echo
          
          mov  asciiChar, al       ; ASCII char to be converted to hex
         
          $AsciiToHex  asciiChar, hexStr2

          mov  ax, word ptr [hexStr2]      ; get hex str
          mov  word ptr [printStr + 6], ax ; move hex str into the print line

Writeln:
          mov  ah, $DOSPSF        ; DOS print string function
          mov  dx, offset newLine ; print string
          int  $DOSIFH            ; print the string

DisplayString:                    
          mov  ah, $DOSPSF         ; DOS print string function
          mov  dx, offset printStr ; print string
          int  $DOSIFH             ; print the sting

Exit:                                   
          mov  ah, $DOSTPF ; DOS terminate program function
          mov  al, 0       ; set exit code
          int  $DOSIFH     ; terminate the program
          endp Main        ; end procedure
          end  start       ; source code end