Notes of HexdumpAT&T


print_byte_as_hex:

    # Print a byte in hex form.
    movl heap_base, %esi
    movb (%esi, %edi, 1), %al
    pushl %eax
    pushl $hex_fmt_str
    call printf
    addl $8, %esp


Heap base: last valid memory address
.long 0





hex_fmt_str:
	.ascii " %02X\0"