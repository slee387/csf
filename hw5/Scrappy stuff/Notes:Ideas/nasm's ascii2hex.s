; from turtle13 on the Forum

; since Nasm will do many things, I like to put the command line in the file
; Nasm -f elf32 myprog.asm
; ld -o myprog muprog.o -m elf_i386

bits 32

section .text

global _start

_start:

        mov ebx, 0              ; set up stdin for keyboard input
        mov eax, 3              ; sys call for read- returns number of bytes typed into eax

; need a buffer to read  into in ecx
        mov ecx, input_buffer

; and the (maximum) count
        mov edx, 200
        int 0x80                ; kernel trap
        ; should be syscall instead of this interrupt 

        mov esi, input_buffer
        mov edi, output_buffer
        mov ecx, eax ; count of bytes typed
        mov ebx, hex_table
        xor edx, edx ; counter for output bytes to print

; sys_read does this
;returnkey:                      ; loop that will terminate the input if return is pressed
 ;       mov

top:
; get a byte/character into al
        lodsb
        push eax ; save a copy
; isolate high nibble
        shr al, 4
        xlat ; alias of xlatb
        stosb ; put it in output buffer
        pop eax ; get our byte back
        and al, 0x0F ; isolate low nibble
        xlatb ; alias of xlat - take your pick
        stosb ; put in output buffer
        mov al, ' ' ; a space
        stosb
        add edx, 3
        loop top  ; do em all
        mov al, 10 ; linefeed
        stosb
        inc edx

; now print it out
        mov eax, 4 ; sys_write
        mov ebx, 1 ; stdout
        mov ecx, output_buffer
        ; edx should be all set
        int 0x80

done:
        mov ebx, 0
        mov eax, 1
        int 0x80

section .data
hex_table db "0123456789ABCDEF"

section .bss

input_buffer resb 200   ; declare 200 bytes for keyboard input
output_buffer resb 601 ; 3 bytes for each input byte, plus 1 for the final linefeed