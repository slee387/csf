/*
 * hexFuncs.S
 * Soo Hyun Lee
 * slee387@jhu.edu
 *
 * This file should implement functions to be called from your
 * main function (in hex.S).
 *
 * You can (and should!) write unit tests for these functions, to
 * the extent possible.  See hexTests.c
 */

	.section .text

/*
 * line2hex
 *
 * Parameters:
 * 	 start - the number to print (will receive from %rsi)
 *   hex_table - in %rcx (the lookup table)
 */
	.globl line2hex

/**
line2hex:
	subq $8, %rsp
	push %rbx 
	push %rdx

	movq %rcx, %r12 	// load hex table 
	movq %rsi, %r13 	// load input_buf 
	push %r13 			// save this input_buf char 
	movq $0, %r9		// counter 

.L_loop:
	// shift 4 bits
	movq $4, %rcx
	shrq %cl, %r13
	xlatb 				// lookup 
	movb %r13b, %dil 	// load the high 4 bits of hex into %rdi 
	inc %r9
	pop %r13

	cmpq $7, %r9
	jne .L_loop


	andb %r13b, 0x0F 	// isolat lower 4 bits 
	xlatb 				// lookup 
	movb %r13b, %dil 	// load this to higher 4 bits 

	// is it moving by 4, still need to add space
	movq %rdi, %rax 	// move %rdi to ret val 


	pop %rdx 
	pop %rbx
	addq $8, %rsp 
	ret 
*/

/*
 * Determine the length of specified character string.
 *
 * Parameters:
 *   s - pointer to a NUL-terminated character string
 *
 * Returns:
 *    number of characters in the string
 * In C: long strLen(const char *s);
 */
	.globl strLen
strLen:
	subq $8, %rsp                 /* adjust stack pointer */
	movq $0, %r10                 /* initial count is 0 */

.LstrLenLoop:
	cmpb $0, (%rdi)               /* found NUL terminator? */
	jz .LstrLenDone               /* if so, done */
	inc %r10                      /* increment count */
	inc %rdi                      /* advance to next character */
	jmp .LstrLenLoop              /* continue loop */

.LstrLenDone:
	movq %r10, %rax               /* return count */
	addq $8, %rsp                 /* restore stack pointer */
	ret

/*
 * Print a C character string to stdout.
 *
 * Parameters:
 *   s - the string to print
 */
	.globl printStr

printStr:
	pushq %r12                    /* preserve contents of %r12 */

	/* determine length of string */
	movq %rdi, %r12               /* save s (strLen will modify %rdi) */
	callq strLen                  /* determine length of s */

	/* use write system call to print string */
	movq $1, %rdi                 /* first write arg is fd (1=stdout) */
	movq %r12, %rsi               /* second write arg is buffer */
	movq %rax, %rdx               /* third write arg is count */
	movq $1, %rax                 /* write is system call 1 */
	syscall                       /* call write */

	popq %r12                     /* restore contents of %r12 */
	ret



/*
 * Converts a byte of char into hex using a lookup table 
 *
 * Parameters:
 *   input_buf - in %rsi (the char to convert)
 *   hex_table - in %rcx (the lookup table)
 *
 * Returns: 
 * 	 output_buf - would contain hex equivalent of char 
 */
	.globl char2hex

char2hex:

	subq $8, %rsp
	push %rbx 
	push %rdx

	movq %rcx, %r12 	/* load hex table */
	movq %rsi, %r13 	/* load input_buf */
	push %r12 			/* save this input_buf char */

	movq $4, %rcx
	shrq %cl, %r13

	//shrq 4, %r13 		/* high 4 bits extracted */
	xlatb 				/* lookup */
	movb %r13b, %dil 	/* load the high 4 bits of hex into %rdi */
	andb %r13b, 0x0F 	/* isolat lower 4 bits */
	xlatb 				/* lookup */
	movb %r13b, %dil 	/* load this to higher 4 bits */

	// is it moving by 4, still need to add space
	movq %rdi, %rax 	/* move %rdi to ret val  */


	pop %rdx 
	pop %rbx
	addq $8, %rsp 
	ret

/*
 * Checks if the char is printable.
 *
 * Parameters:
 *   input_buf - in %rsi (the char to check)
 * 
 * Returns:
 * 	 output_buf - either returns char as is or replaces it with '.'
 */
	.globl isPrint
isPrint:
	
	subq $8, %rsp 				  /* Adjust stack pointer */
	movq %rsi, %r12               /* Save input_buf into %r12 */

	cmpq $32, %r12				  /* If input_buf char < 32 */
	jl .Ldot					  /* Replace with . by calling function */
	cmpq $126, %r12 			  /* If input_buf char > 126 */
	jg .Ldot					  /* Replace with . by calling function */
	movq %r12, %r13				  /* Else put the original char into r13*/

.Ldot: 
	movq $'.', %r13				  /* Replace char with . if condition met*/
	jmp .Ldone


.Ldone: 
	movq %r13, %rax				  /* Copy over char to ret register rax */
	addq $8, %rsp				  /* Readjust stack pointer */
	ret

/*
 * addLongs function (this is just an example function)
 */
	.globl addLongs
addLongs:
	subq $8, %rsp                 /* align stack pointer */

	movq %rsi, %rax               /* move second parameter to %rax */
	addq %rdi, %rax               /* add first parameter to second */
	/* the sum is now in %rax */

	addq $8, %rsp                 /* restore stack pointer */
	ret

/* vim:ft=gas:
 */
